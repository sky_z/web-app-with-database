package com.ko.springdemo.dao;

import java.util.List;

import com.ko.springdemo.entity.Customer;

public interface CustomerDAO {

	/**
	 * For getting a List of Customers
	 * @return a List filled with Customer Objects
	 */
	public List<Customer> getCustomers();

	/**
	 * Saving a Customer
	 * @param theCustomer
	 */
	public void saveCustomer(Customer theCustomer);

	public Customer getCustomer(int theId);

	public void deleteCustomer(int theId);

}
